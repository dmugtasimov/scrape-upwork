.PHONY: dev-run
dev-run:
	poetry run python -m scrape_upwork.django.manage runserver 127.0.0.1:8001

.PHONY: debug-scrape
debug-scrape:
	poetry run scrapy crawl job -a page_limit=1

.PHONY: scrape
scrape:
	poetry run scrapy crawl job

.PHONY: dev-migrate
dev-migrate:
	poetry run python -m scrape_upwork.django.manage migrate

.PHONY: dev-deps-up
dev-deps-up:
	docker-compose up
