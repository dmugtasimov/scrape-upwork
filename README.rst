Local development environment
=============================

This section describes how to setup development environment for Linux Mint 18.3.

Initial setup
+++++++++++++

Once initial setup is done only corresponding `Upgrade`_ section should be performed
to get the latest version for development.

#. Install PyCharm to use as IDE
#. Install prerequisites::

    apt update
    apt install git

#. [if you have not configured it globally] Configure git::

    git config user.name 'Firstname Lastname'
    git config user.email 'youremail@youremail_domain.com'

#. Install Docker according to https://docs.docker.com/install/linux/docker-ce/ubuntu/
   (known working: Docker version 19.03.2, build 6a30dfc)

#. Add your user to docker group::

    sudo usermod -aG docker $USER
    exit

#. Install Docker Compose according to https://docs.docker.com/compose/install/
   (known working: docker-compose version 1.24.1, build 4667896b)

#. Install prerequisites (
    as prescribed at https://github.com/pyenv/pyenv/wiki/Common-build-problems )::

    apt update
    apt install make build-essential libssl-dev zlib1g-dev libbz2-dev \
                libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
                libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl

#. Install and configure `pyenv` according to https://github.com/pyenv/pyenv#basic-github-checkout
#. Install Python 3.8.2::

    pyenv install 3.8.2

#. Install `pip`::

    # Ensure you are in `daily-trader-core` directory
    pip install pip==20.0.2

#. Install Poetry::

    pip install --user poetry

#. Configure Poetry to use $HOME/.virtualenvs::

    poetry config virtualenvs.path $HOME/.virtualenvs

#. Create virtualenv with Poetry::

    poetry shell

#. Install pip::

    pip install pip==20.0.2

#. Run dependencies with Docker Compose (in a separate terminal)::

    docker-compose dev-deps-up

#. Continue to `Update`_ section
#. Create superuser::

    python -m scrape_upwork.django.manage createsuperuser

Update
++++++

#. Activate shell::

    poetry shell

#. Update dependencies::

    poetry install

#. Install Daily Trader Core in development mode::

    pip install -e .

#. Run migrations::

    make dev-migrate

Run
+++

#. Run dependencies with Docker Compose (in a separate terminal)::

    make dev-deps-up

#. Run server::

    make dev-run
