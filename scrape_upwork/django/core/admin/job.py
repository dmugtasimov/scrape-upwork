from django.contrib import admin
from django.utils.html import format_html

from scrape_upwork.django.core.models import Job

URL_TEMPLATE = 'https://www.upwork.com/jobs/{ciphertext}/'


@admin.register(Job)
class JobAdmin(admin.ModelAdmin):
    list_display = ('uid', 'country', 'title', 'hide', 'link', 'hourly_rate_min',
                    'hourly_rate_max', 'created_at_original')
    readonly_fields = ('link', 'created_at',)
    list_filter = ('is_hidden', 'country')

    class Media:
        js = ('js/hide.js',)

    def link(self, obj):
        url = URL_TEMPLATE.format(ciphertext=obj.ciphertext)
        return format_html(f'<a href="{url}" target="_blank">{url}</a>')

    def hide(self, obj):
        if obj.is_hidden:
            return format_html('<span>Hidden</span>')

        obj_id = obj.id
        return format_html(f'<button id="hide-button-{obj_id}" type="button" class="btn btn-primary" '
                           f'onclick="hide({obj_id});">Hide</button>')
