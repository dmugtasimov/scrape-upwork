from django.db import models
from django.contrib.postgres.fields import JSONField


class Job(models.Model):
    uid = models.CharField(max_length=20, unique=True)
    title = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)
    created_at_original = models.DateTimeField()
    country = models.CharField(max_length=64)
    hourly_rate_min = models.SmallIntegerField()
    hourly_rate_max = models.SmallIntegerField()
    ciphertext = models.CharField(max_length=20)
    raw = JSONField()

    is_hidden = models.BooleanField(default=False)
