function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = django.jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function hide(jobId) {
    django.jQuery.ajax({
        type: 'POST',
        url: '/admin/core/job/' + jobId + '/hide/',
        contentType: 'application/json; charset=utf-8',
        headers: {
            'X-CSRFToken': getCookie('csrftoken')
        },
        success: function(data, textStatus, jqXHR) {
            django.jQuery('button#hide-button-' + jobId).replaceWith('<span>Hidden</span>');
        }
    });
}
