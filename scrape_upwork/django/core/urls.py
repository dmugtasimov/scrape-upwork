from django.urls import path

from scrape_upwork.django.core.views.job import HideJobView


urlpatterns = [
    path('core/job/<int:job_id>/hide/', HideJobView.as_view()),
]
