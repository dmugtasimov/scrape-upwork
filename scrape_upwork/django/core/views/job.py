from django.http import HttpResponse
from django.views import View

from scrape_upwork.django.core.models import Job


class HideJobView(View):
    def post(self, request, job_id):
        job = Job.objects.select_for_update().only('is_hidden').get(id=job_id)
        job.is_hidden = True
        job.save()
        return HttpResponse()
