from django.views.generic.base import RedirectView
from django.contrib import admin
from django.urls import path, include

import scrape_upwork.django.core.urls


urlpatterns = [
    path('admin/', include(scrape_upwork.django.core.urls)),
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(pattern_name='admin:index')),
]
