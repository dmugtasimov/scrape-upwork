from scrapy_djangoitem import DjangoItem

from scrape_upwork.django.core.models import Job


class JobItem(DjangoItem):
    django_model = Job
