from scrape_upwork.django.core.models import Job


class JobPipeline:
    def process_item(self, item, spider):
        if not Job.objects.filter(ciphertext=item['ciphertext']).exists():
            item.save()

        return item
