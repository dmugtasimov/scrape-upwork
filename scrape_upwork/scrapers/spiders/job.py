from http.cookies import SimpleCookie
import json
from datetime import datetime
from decimal import Decimal
from urllib.parse import urlencode

import scrapy
from scrapy.http import Request

from scrape_upwork.scrapers.items.job import JobItem


BASE_URL = 'https://www.upwork.com/ab/jobs/search/url'
ARGUMENTS_TEMPLATE = {
    'page': NotImplemented,
    'per_page': 50,
    'sort': 'recency',
    'contractor_tier': 3,  # expert level
    't': 0,  # hourly
}


def make_url(page):
    return '{}?{}'.format(BASE_URL, urlencode(dict(ARGUMENTS_TEMPLATE, page=page)))


def get_cookie_value(response, name):
    for header_value in response.headers.getlist('Set-Cookie'):
        simple_cookie = SimpleCookie()
        simple_cookie.load(header_value.decode())
        for key, value in simple_cookie.items():
            if key == name:
                return value.value


class JobSpider(scrapy.Spider):
    name = 'job'
    custom_settings = {
        'ITEM_PIPELINES': {
            'scrape_upwork.scrapers.pipelines.job.JobPipeline': 300,
        },
        'DOWNLOAD_DELAY': 5,
    }

    def __init__(self, *args, page_start=1, page_limit=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.page_limit = None if page_limit is None else int(page_limit)
        self.page_start = int(page_start)

        self._cookies = None

    def make_cookies(self):
        if self._cookies is None:
            with open('local/cookies.json') as f:
                self._cookies = json.load(f)['Request Cookies']

        return self._cookies

    def make_headers(self):
        return {
            'X-Odesk-Csrf-Token': self.make_cookies()['XSRF-TOKEN'],
            'X-Odesk-User-Agent': 'oDesk LM',
            'X-Requested-With': 'XMLHttpRequest',
        }

    def start_requests(self):
        page = self.page_start
        yield Request(url=make_url(page), cookies=self.make_cookies(), headers=self.make_headers(),
                      callback=self.parse_job_list, meta={'page': page})

    def parse_job_list(self, response):
        response_json = json.loads(response.body.decode())
        search_results = response_json.get('searchResults')
        if not search_results:
            return

        jobs = search_results.get('jobs')
        if not jobs:
            return

        for job in jobs:
            hourly_budget_text = job.get('hourlyBudgetText')
            if not hourly_budget_text:
                continue

            parts = [Decimal(rate.lstrip('$')) for rate in hourly_budget_text.split('-')]
            parts_len = len(parts)
            if parts_len == 2:
                hourly_rate_min, hourly_rate_max = parts
            elif parts_len == 1:
                hourly_rate_min = hourly_rate_max = parts[0]
            else:
                self.logger.warning('Unexpected "hourlyBudgetText" format: %s', hourly_budget_text)
                return

            created_at_original = job.get('createdOn')
            if created_at_original:
                created_at_original = datetime.fromisoformat(created_at_original)

            yield JobItem(
                uid=job.get('uid'),
                title=job.get('title'),
                created_at_original=created_at_original,
                country=((job.get('client') or {}).get('location') or {}).get('country') or '-',
                hourly_rate_min=hourly_rate_min,
                hourly_rate_max=hourly_rate_max,
                ciphertext=job.get('ciphertext'),
                raw=job,
            )

        if jobs:
            page = response.meta.get('page') + 1
            if self.page_limit is not None and page > self.page_start + self.page_limit:
                return

            yield Request(url=make_url(page), cookies=self.make_cookies(),
                          headers=self.make_headers(), callback=self.parse_job_list,
                          meta={'page': page})
