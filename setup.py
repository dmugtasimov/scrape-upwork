from setuptools import setup

setup(
    name='scrape-upwork',
    version='0.1.0',
    description='Scrape Upwork',
    packages=['scrape_upwork'],
    zip_safe=False,
    include_package_data=True,
)
